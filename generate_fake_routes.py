import asyncio
from random import randint

import aiohttp
import requests

ROUTE_URL = 'http://localhost:3000/api/v1/route'
USER_URL = 'http://localhost:3001/api/v1/user'


def get_token(login: str = 'User2'):
    """Получение токена одного из пользователей.
    """
    creds = {"login": login, "password": "qwerty123"}
    res = requests.post(f'{USER_URL}/authenticate', json=creds)
    return res.json().get('access_token')


async def generate_routes():
    """Генератор маршрутов.
    """
    for i in range(200):
        yield {
            "title": f"route-{i}",
            "start_point_id": randint(1, 100_000),
            "end_point_id": randint(1, 100_000),
        }


async def do_post(session, url, data):
    async with session.post(url, json=data) as response:
        print(f"Request complete!")


async def create_routes(token):
    session_timeout = aiohttp.ClientTimeout(total=None)
    async with aiohttp.ClientSession(
            timeout=session_timeout,
            headers={'Authorization': f'Bearer {token}'}
    ) as session:
        post_tasks = []
        async for data in generate_routes():
            post_tasks.append(do_post(session, ROUTE_URL, data))

        await asyncio.gather(*post_tasks)


if __name__ == '__main__':
    token = get_token()
    print(f"token = {token}")

    if token:
        # Добавление маршрутов после добавления всех точек
        loop = asyncio.get_event_loop()
        try:
            loop.run_until_complete(create_routes(token))
        finally:
            loop.close()
