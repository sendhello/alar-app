# Alar TestApp

Сервис построения маршрутов.

### Пути к модулям сервиса:

* Alar MapService - https://gitlab.com/sendhello/alar-mapservice
* Alar UserService - https://gitlab.com/sendhello/alar-userservice
* Alar RouteCreator - https://gitlab.com/sendhello/alar-routecreator

### Запуск сервиса
```shell
docker-compose -f docker-compose.yml up
```

### Swagger Документация
* http://localhost:3000/docs
* http://localhost:3001/docs

## API

### Модуль пользователей

#### Создание пользователя
###### Request
```json
POST http://localhost:3001/api/v1/user
{
  "login": "string",
  "password": "string"
}
```
###### Response
```json
{
  "login": "string",
  "id": 1
}
```

#### Получение списка пользователей
###### Request
```json
GET http://localhost:3001/api/v1/user
```
###### Response
```json
[
  {
  "login": "string",
  "id": 1
  },
  ...
]
```

#### Получение пользователя по ID
###### Request
```json
POST http://localhost:3001/api/v1/user/<user_id:int>
```
###### Response
```json
{
  "login": "string",
  "id": 1
}
```

#### Аутентификация пользователя и получение токена
###### Request
```json
POST http://localhost:3001/api/v1/user/authenticate
{
  "login": "string",
  "password": "string"
}
```
###### Response
```json
{
  "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJsb2dpbiI6Ik5hbWUiLCJleHAiOjE2NDY4NDA1MDR9.eXg9DcX2CMFfFNj8Ro51XtejoEZ5K-0mVhQhYRMdUKQ",
  "token_type": "Bearer"
}
```

#### Валидация токена
###### Request
```json
POST http://localhost:3001/api/v1/user/validate
{
  "access_token": "string"
}
```
###### Response
```json
{
  "valid": true,
  "user": {
    "login": "string",
    "id": 0
  }
}
```

### Основной модуль

### Точки
#### Создание точки
###### Request
```json
POST http://localhost:3001/api/v1/point
{
  "title": "string",
  "pos_x": 0,
  "pos_y": 0
}
```
###### Response
```json
{
  "title": "string",
  "pos_x": 0,
  "pos_y": 0,
  "id": 1
}
```

#### Получение списка точек
###### Request
```json
GET http://localhost:3001/api/v1/point
```
###### Response
```json
[
  {
    "title": "string",
    "pos_x": 0,
    "pos_y": 0,
    "id": 1
  }
]
```

#### Получение точки по ID
###### Request
```json
POST http://localhost:3001/api/v1/point/<point_id:int>
```
###### Response
```json
{
  "title": "string",
  "pos_x": 0,
  "pos_y": 0,
  "id": 1
}
```

### Маршруты
#### Создание маршрута
###### Request
```json
POST http://localhost:3001/api/v1/route
{
  "title": "string",
  "start_point_id": 1,
  "end_point_id": 4
}
```
###### Response
###### При создании маршрута список нод всегда null
```json
{
  "title": "11",
  "id": 34,
  "nodes": null
}
```

#### Получение списка маршрутов
###### Request
```json
GET http://localhost:3001/api/v1/route
```
###### Response
```json
[
  {
    "title": "11",
    "id": 2,
    "user": {
      "id": 1,
      "login": "User1"
    },
    "nodes": [
      {
        "route_id": 34,
        "pos": 1,
        "point_id": 1
      },
      {
        "route_id": 34,
        "pos": 2,
        "point_id": 1
      },
      {
        "route_id": 34,
        "pos": 3,
        "point_id": 4
      },
      {
        "route_id": 34,
        "pos": 4,
        "point_id": 4
      }
    ]
  },
  ...
]
```

#### Получение маршрута по ID
###### Request
```json
POST http://localhost:3001/api/v1/route/<route_id:int>
```
###### Response
```json
{
  "title": "11",
  "id": 2,
  "user": {
    "id": 1,
    "login": "User1"
  },
  "nodes": [
    {
      "route_id": 34,
      "pos": 1,
      "point_id": 1
    },
    {
      "route_id": 34,
      "pos": 2,
      "point_id": 1
    },
    {
      "route_id": 34,
      "pos": 3,
      "point_id": 4
    },
    {
      "route_id": 34,
      "pos": 4,
      "point_id": 4
    }
  ]
}
```

### Отчеты
#### Получение отчета по маршрутам
###### Request
```json
POST http://localhost:3001/api/v1/report/routes_count>
```
###### Response
```json
[
  {
    "login": "User1",
    "routes_count": 25,
    "routes_length": 126
  },
  ...
]
```

### Healthcheck
###### Request
```json
POST http://localhost:3001/api/v1/healthcheck
```
###### Response
```json
{
  "status": "alive"
}
```


## Заполнение фейковыми данными:

### Зависимости
* python 3.10+
* poetry

### Установка виртуального окружения и зависимостей:
```shell
poetry install
```

### Заполнение тестовыми данными
##### Добавление точек
(!) Генерация точек занимает ~20 мин
```shell
python generate_fake_points
```
##### Добавление маршрутов
```shell
python generate_fake_data
```
