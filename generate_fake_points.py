import asyncio
from random import randint

import aiohttp
import requests

POINT_URL = 'http://localhost:3000/api/v1/point'
USER_URL = 'http://localhost:3001/api/v1/user'

USERS_COUNT = 5
POINTS_BOX = 3_000
ALL_POINTS = 1_000_000


def create_users():
    """Создание пользователей.
    """
    for i in range(USERS_COUNT):
        creds = {"login": f"User{i}", "password": "qwerty123"}
        requests.post(USER_URL, json=creds)


def get_token(login: str = 'User1'):
    """Получение токена одного из пользователей.
    """
    creds = {"login": login, "password": "qwerty123"}
    res = requests.post(f'{USER_URL}/authenticate', json=creds)
    return res.json().get('access_token')


async def generate_points():
    """Генератор координат.
    """
    points = set()
    for i in range(ALL_POINTS):
        while True:
            point = (randint(1, 100_000), randint(1, 100_000))
            if point in points:
                continue

            points.add(point)
            break

    suffix = 0
    points_box = []
    for point in points:
        suffix += 1
        points_box.append({
            "title": f"point_{suffix}",
            "pos_x": point[0],
            "pos_y": point[1]
        })
        if len(points_box) == POINTS_BOX:
            to_send = points_box
            points_box = []
            yield to_send


async def do_post(session, url, data):
    async with session.post(url, json=data) as response:
        print(f"Request complete!")


async def create_points(token):
    session_timeout = aiohttp.ClientTimeout(total=None)
    async with aiohttp.ClientSession(
            timeout=session_timeout,
            headers={'Authorization': f'Bearer {token}'},
    ) as session:
        post_tasks = []
        async for data in generate_points():
            post_tasks.append(do_post(session, POINT_URL, data))

        await asyncio.gather(*post_tasks)


if __name__ == '__main__':

    create_users()
    token = get_token()
    print(f"token = {token}")

    if token:
        loop = asyncio.get_event_loop()
        try:
            loop.run_until_complete(create_points(token))
        finally:
            loop.close()
